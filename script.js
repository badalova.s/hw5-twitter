let userContainer = document.getElementById('userContainer');
class Card {
    constructor (name,email,id,title,body){
        this.name=name
        this.email=email
        this.id=id
        this.title=title
        this.body=body   
    }
        createUser () {
            let divUser= document.createElement("div")
            divUser.setAttribute('data-id',this.id)
            divUser.style.background="black"
            
            let pName=document.createElement("p")
            pName.innerHTML=this.name
            pName.style.fontSize="22px"
            let pEmail=document.createElement("p")
            pEmail.innerHTML=`  email: ${this.email}`

        
            divUser.appendChild(pName);
            divUser.appendChild(pEmail);
            userContainer.appendChild(divUser)
            
            let pTitle=document.createElement("p")
            pTitle.innerHTML=this.title
            pTitle.style.textTransform="Capitalize"
            let pBody=document.createElement("p")
            pBody.innerHTML=this.body
            pBody.style.textTransform="Capitalize"
            divUser.appendChild(pTitle);
            divUser.appendChild(pBody);  
            const deleteButton = document.createElement('button');
            deleteButton.textContent = 'Delete';
            deleteButton.setAttribute('data-id',`button${this.id}`)
            divUser.appendChild(deleteButton);  

        }

}

function createCard() {
fetch(`https://ajax.test-danit.com/api/json/users`)
.then(response => response.json())
.then(us => {

  us.map(el=> {
    fetch(`https://ajax.test-danit.com/api/json/users/${el.id}/posts`)
    .then(response => response.json())
    .then(post=> {
        post.map(p=> {
      let user = new Card (el.name,el.email,el.id,p.title,p.body)      
      user.createUser()

        }) 
        const botton = document.querySelector(`[data-id='button${el.id}']`)
        botton.addEventListener('click', () => {
         deleteCard(el.id);
        
         }) });
  })
})
.catch(error => console.log(error))
}

createCard()

function deleteCard(id) {
    if (confirm('Are you sure you want to delete this post?')) {
      fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
        method: 'DELETE',
      })
        .then(() => {
          alert('Post deleted');
          const div = document.querySelector(`[data-id='${id}']`)
            userContainer.removeChild(div)
        })
    }
}

